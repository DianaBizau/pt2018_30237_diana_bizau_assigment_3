package Presentation;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import Model.Order;

public class OrderTable 
{
	public static DefaultTableModel constructTable(ArrayList<Order> o)
	{
		 DefaultTableModel model = new DefaultTableModel();
		 model.addColumn("idcomanda");
		 model.addColumn("idprodus");
		 model.addColumn("idClient");
		 model.addColumn("cantitate");
		 model.addColumn("Total");
  
		 Table t = new Table();
   
		 for(Order cl: o)
			 t.retrivePr(cl, model);
		 	return model; 
	 }

}
