package Presentation;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import Model.Product;

public class ProductTable 
{
	public static DefaultTableModel constructTable(ArrayList<Product> p)
	{
		 DefaultTableModel model = new DefaultTableModel();
		 model.addColumn("idprodus");
		 model.addColumn("nume");
		 model.addColumn("pret");
		 model.addColumn("cantitate");
   
		 Table t = new Table();
    
		 for(Product cl: p)
			 t.retrivePr(cl, model);
		 	return model;
	 }
}
