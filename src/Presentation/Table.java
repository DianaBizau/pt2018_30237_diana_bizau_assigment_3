package Presentation;

import java.lang.reflect.Field;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class Table  
{
	public DefaultTableModel retrivePr(Object object, DefaultTableModel model)
	{
		Vector<Object> v = new Vector<Object>(); //salvam in vector pentru a putea adauga vectorul in model
		Object value = null;
		for(Field field: object.getClass().getDeclaredFields())
		{
			field.setAccessible(true);
			
			try { value = field.get(object);
	
			v.addElement(value);
	
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		}
		}
		model.addRow(v);	
		return model;
	}
	
}
