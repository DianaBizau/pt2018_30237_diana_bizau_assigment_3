package Presentation;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import  Model.Client;

public class ClientTable 
{ 
	 public static DefaultTableModel constructTable(ArrayList<Client> c)
	 {
		 DefaultTableModel model = new DefaultTableModel();
		 model.addColumn("IdClient");
		 model.addColumn("Nume");
		 model.addColumn("Adresa");
    
		 Table t = new Table();
     
		 for(Client cl: c)
			 t.retrivePr(cl, model);
		 return model;
	 }
}
