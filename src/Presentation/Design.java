package Presentation;

import Model.Client;
import Model.Order;
import Model.Product;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import BussinessLogic.ClientBLL;
import BussinessLogic.OrderBLL;
import BussinessLogic.ProductBLL;

public class Design 
{
	 JTextField name = new JTextField();
	 JTextField id = new JTextField();
	 JTextField address = new JTextField();
	 JTextField nameProduct = new JTextField();
	 JTextField idP = new JTextField();
	 JTextField quantity = new JTextField();
	 JTextField quantityO = new JTextField();
	 JTextField price = new JTextField();
	 JTextField idOrder = new JTextField();
	 JTable table ;
	 JLabel lblidOrd = new JLabel ("Delivery order: ");
	 JLabel lblName = new JLabel("Name");
	 JLabel lblidClient = new JLabel("Id Client");
	 JLabel lblqo = new JLabel("Order Quantity");
	 JLabel lbladdress = new JLabel("Address");
	 JLabel lblPrice = new JLabel("Price");
	 JLabel lblidP = new JLabel("Id Product");
	 JLabel lblProduct = new JLabel("product Name");
	 JLabel lblQuantity = new JLabel("Quantity");
	ClientBLL clientBLL = new ClientBLL();
	ProductBLL productBLL = new ProductBLL();
	OrderBLL orderBLL = new OrderBLL();
	Random ran = new Random();

	public Design() 
	{ 
		initializare();
	}
	
	public void initializare()
	{

		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		table= new JTable();
		table.setBounds(400, 300, 390, 220);
		frame.getContentPane().add(table);
	
		frame.setTitle("WareHouse");

		frame.setSize(800, 580);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		lblName.setBounds(34, 240, 41, 20);
		frame.getContentPane().add(lblName);

		name.setBounds(34, 260, 135, 20);
		frame.getContentPane().add(name);
		name.setColumns(10);

		lbladdress.setBounds(34, 290, 60, 20);
		frame.getContentPane().add(lbladdress);
		
		address.setColumns(10);
		address.setBounds(34, 310, 135, 20);
		frame.getContentPane().add(address);
		
		
		idOrder.setBounds(570, 90, 90, 20);
		frame.getContentPane().add(idOrder);
		
		lblidOrd.setBounds(570, 70, 100, 20);
		frame.getContentPane().add(lblidOrd);
		
		
		lblqo.setBounds(400, 195, 90, 20);
		frame.getContentPane().add(lblqo);
		
		quantityO.setBounds(400, 213, 90, 20);
		frame.getContentPane().add(quantityO);
		
		lblidClient.setBounds(34, 193, 60, 20);
		frame.getContentPane().add(lblidClient);

		id.setColumns(10);
		id.setBounds(34, 213, 135, 20);
		frame.getContentPane().add(id);
		
		lblProduct.setBounds(230, 193, 80, 20);
		frame.getContentPane().add(lblProduct);
		
		nameProduct.setBounds(230, 213, 135, 20);
		frame.getContentPane().add(nameProduct);
		

		lblidP.setBounds(230, 240, 80, 20);
		frame.getContentPane().add(lblidP);
		
		idP.setBounds(230, 260, 135, 20);
		frame.getContentPane().add(idP);
		

		lblQuantity.setBounds(230, 290, 80, 20);
		frame.getContentPane().add(lblQuantity);
	
		quantity.setBounds(230, 310, 135, 20);
		frame.getContentPane().add(quantity);
	

		lblPrice.setBounds(230, 340, 80, 20);
		frame.getContentPane().add(lblPrice);
		
		price.setBounds(230, 360, 135, 20);
		frame.getContentPane().add(price);
	
		
		JButton btnOrder = new JButton("Order");
		btnOrder.setBounds(34, 460, 89, 23);
		frame.getContentPane().add(btnOrder);
		
		JButton btnSearch = new JButton("Search Product");
		btnSearch.setBounds(230, 460, 140, 23);
		frame.getContentPane().add(btnSearch);
		
		JButton button_3 = new JButton("Add Client");
		button_3.setBounds(34, 77, 150, 28);
		frame.getContentPane().add(button_3);
		

		JButton button_4 = new JButton("Remove Client");
		button_4.setBounds(34, 120, 150, 28);
		frame.getContentPane().add(button_4);
		
		JButton button_5 = new JButton("Update Client");
		button_5.setBounds(34, 160, 150, 28);
		frame.getContentPane().add(button_5);
		
		JButton button = new JButton("See Clients");
		button.setBounds(34, 25, 150, 28);
		frame.getContentPane().add(button);
		
		JButton button_6 = new JButton("Add Product");
		button_6.setBounds(230, 77, 150, 28);
		frame.getContentPane().add(button_6);


		JButton button_7 = new JButton("Remove Product");
		button_7.setBounds(230, 120, 150, 28);
		frame.getContentPane().add(button_7);
		
		JButton button_8 = new JButton("Update Product");
		button_8.setBounds(230, 160, 150, 28);
		frame.getContentPane().add(button_8);
		
		JButton button_1 = new JButton("See Products");
		button_1.setBounds(230, 25, 150, 28);
		frame.getContentPane().add(button_1);
		
		JButton button_2 = new JButton("See Orders");
		button_2.setBounds(400, 25, 150, 28);
		frame.getContentPane().add(button_2);
		frame.setVisible(true);
		
		
		JButton button_9 = new JButton("Livrare");
		button_9.setBounds(400, 77, 150, 28);
		frame.getContentPane().add(button_9);
		
		btnOrder.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				ArrayList<Client> clients = null;
				ArrayList<Product> product = null;
				ArrayList<Order> order = null;
				int idclient = Integer.parseInt(id.getText());
				int idproduct = Integer.parseInt(idP.getText());
				int q = Integer.parseInt(quantityO.getText());
				try {
					clients = clientBLL.listare();
					product = productBLL.listare();
					
					float total = 0;
					int diferenta = 0;
					String nameP = "";
					String nameC = "";
					float price = 0;
					int ok = 0;
					for (Product p :product)
					{
						if (p.getId() == idproduct)
						{
							if (p.getQuantity() < q)
							{
								ok = 1;
								String s ="Not enough quantity!";
								JOptionPane.showMessageDialog(null, s);
							}
							else
							{
							total = p.getPrice() * q;
							diferenta = p.getQuantity() - q;
							nameP = p.getName();
							price = p.getPrice();
							}
						}
					}
					for (Client c:clients)
					{
						if (c.getId() == idclient)
						{
							nameC = c.getName();	
						}
					}
					float totalprice = 0;
					if (ok != 1){
					productBLL.update(nameP, diferenta , price, idproduct);
					
					int x = ran.nextInt((100 - 1 ) + 1) +1;
					orderBLL.insert(idproduct, idclient, q, x, total);
					order = orderBLL.listare();
		
				PrintWriter writer = new PrintWriter("ORDER" + x + ".txt", "UTF-8");
					writer.println("Order Details:");
					writer.println("Client Name: " + nameC);
					writer.println("Products and information:");
					for (Order c:order)
					{
						if (c.getIdClient() == idclient )
						{
							for (Product c2:product)
							{
								if (c2.getId() == c.getIdProduct() )
								{
									writer.println("Produs: "+ c2.getName() + "   Pret: "+ c2.getPrice() + "   Cantitate: " + c.getQuantity() + "   Total Price: "+ c.getTotal());
									totalprice = totalprice + c.getTotal();
								}
							}
						}
					}
					writer.println("Final Price: " + totalprice);
					writer.close();
					}
				} catch (FileNotFoundException e1){
					e1.printStackTrace();
				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}	
			}
		});
		button_9.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				int id1 = Integer.parseInt(idOrder.getText());
				
				orderBLL.delete(id1);	
			}
		});	
		button_2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				ArrayList<Order> order = null;
							order = orderBLL.listare();	
							
							DefaultTableModel model = new DefaultTableModel();
							model = OrderTable.constructTable(order);
				
							table.setModel(model);

							for(Order o : order)
								System.out.println(o);		
			}
		});
		btnSearch.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				ArrayList<Product> p = null;
				String n = nameProduct.getText();
							p = productBLL.search(n);	
					if(p.isEmpty())
					{	String s = "Nu detinem produsul cautat!";
					JOptionPane.showMessageDialog(null, s);}
					else	
						{
						DefaultTableModel model = new DefaultTableModel();
						model = ProductTable.constructTable(p);
						
						table.setModel(model);
						for(Product o : p)
								System.out.println(o);
						}	
			}
		});	
		button_3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				String n = name.getText();
				String a = address.getText();
				int idC = Integer.parseInt(id.getText());

				clientBLL.insert(idC, n, a);	
			}

		});	
		button_4.addActionListener(new ActionListener()
		{			
			public void actionPerformed(ActionEvent e) 
			{
					int id1 = Integer.parseInt(id.getText());	
					clientBLL.delete(id1);
			}
		});		
		button_5.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
					int id1 = Integer.parseInt(id.getText());
					String n = name.getText();
					String a = address.getText();
					clientBLL.update(id1, n, a);
			}

		});	
		button.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				ArrayList<Client> clienti = null;
				clienti = clientBLL.listare();
				DefaultTableModel model = new DefaultTableModel();
				model = ClientTable.constructTable(clienti);
				table.setModel(model);		
				for(Client c : clienti)
					System.out.println(c);	
			}
		});
		button_1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				ArrayList<Product> products = null;
				products = productBLL.listare();
				DefaultTableModel model = new DefaultTableModel();
				model = ProductTable.constructTable(products);
				table.setModel(model);
				for(Product p : products)
					System.out.println(p);
			}
		});
		button_6.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				int id = Integer.parseInt(idP.getText());
				String name = nameProduct.getText();
				float pr =  Float.parseFloat(price.getText());
				int q = Integer.parseInt(quantity.getText());
				productBLL.insert( name, q, pr,id);	
			}
		});
		button_7.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				int id1 = Integer.parseInt(idP.getText());
				productBLL.delete(id1);
			}
		});
		button_8.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				int id1 = Integer.parseInt(idP.getText());
				String name = nameProduct.getText();
				float p = Float.parseFloat(price.getText());
				int q = Integer.parseInt(quantity.getText());
				productBLL.update(name, q,p,id1);		
			}
		});			
	}
}

