package Model;

public class Client 
{
	private int idClient;
	private String name, address;
	
	public Client(int idClient, String name, String address)
	{
		this.idClient = idClient;
		this.name = name;
		this.address = address;
		
	}
	
	public int getId()
	{
		return idClient;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public String toString()
	{
		return "Name " + getName() + " ID " + getId() + " Address " + getAddress();
	}

}
