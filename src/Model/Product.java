package Model;

public class Product 
{
	private int id;
	private String name;
	private float price;
	private int quantity;
	
	public Product(int id,String name,float price,int quantity)
	{
		this.id=id;
		this.name=name;
		this.price=price;
		this.quantity = quantity;
	}
	
	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}
	
	public float getPrice()
	{
		return price;
	}

	public int getQuantity()
	{
		return quantity;
	}
	
	@Override
	public String toString()
	{
		return String.format("Products [id=%s, name=%s, price=%s, quantity=%s]", id,name,price,quantity);
	}

}
