package Model;

public class Order
{
	private int id;
	private float total;
	private int id_product;
	private int id_client;
	private String quantity;
	
	public Order(int id,int id_product,int id_client,String quantity,float total)
	{
		this.id=id;
		this.id_product=id_product;
		this.id_client=id_client;
		this.quantity=quantity;
		this.total=total;	
	}
	
	public int getId()
	{
		return id;
	}
	
	public int getIdProduct()
	{
		return id_product;
	}
	
	public int getIdClient()
	{
		return id_client;
	}

	public float getTotal()
	{
		return total;
	}

	public String getQuantity()
	{
		return quantity;
	}
	
	@Override
	public String toString()
	{
		return String.format("Order [id=%s, idp=%s, idc=%s, quantity=%s, total=%s]", id,id_product,id_client,quantity,total);
	}


}
