package BussinessLogic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import DataAcces.ConnectionFactory;
import Model.Order;


public class OrderBLL
{
	protected static final Logger LOGGER = Logger.getLogger(OrderBLL.class.getName());
	private static final String insertStatementString = "INSERT INTO comanda (idcomanda,idProdus, idClient, cantitate,total)" + " VALUES (?,?,?,?,?)";
	private final static String listStatementString = "SELECT* FROM comanda";
	private final static String deleteStatementString = "DELETE FROM comanda where idcomanda = ?";
	private final static String updateStatementString = "UPDATE comanda set idProdus=(?),idClient=(?),cantiate=(?),total=(?) where idcomanda=(?)";
	
	
	public void insert(int idProdus,int idClient,int cantitate, int idcomanda,float total)
	{
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
	
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, idProdus);
			insertStatement.setInt(2, idClient);
			insertStatement.setInt(3, cantitate);
			insertStatement.setInt(4, idcomanda);
			insertStatement.setFloat(5, total);
			insertStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderBLL:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		

}
	
	public void delete(int idcomanda)
	{
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement = null;
	
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, idcomanda);
			int rowsDeleted  = deleteStatement.executeUpdate();
			if(rowsDeleted > 0) System.out.println("Delete");
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderBLL:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
			
		}
	}
	
	public void update (int idcomanda,int idProdus,int idClient,int cantitate,int total)
	{
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement updateStatement = null;
		
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(1,idProdus);
			updateStatement.setInt(2, idClient);
			updateStatement.setInt(3, cantitate);
			updateStatement.setInt(4, total);
			updateStatement.setInt(5, idcomanda);
			int rowsUpdated = updateStatement.executeUpdate();
			if(rowsUpdated > 0 ) System.out.println("Update");
		
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientBLL:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		
	}
	
	public ArrayList<Order>listare()
	{
		ArrayList<Order> list = new ArrayList<Order>();
		
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement listStatement = null;
		ResultSet rs = null;
	
		try {
			listStatement = dbConnection.prepareStatement(listStatementString, Statement.RETURN_GENERATED_KEYS);
			
			rs = listStatement.executeQuery();
			while(rs.next())
			{
			
				int idProdus = rs.getInt("idProdus");
				int idClient = rs.getInt("idClient");
				String cantitate = rs.getString("cantitate");
				float total = rs.getFloat("total");
				int idcomanda = rs.getInt("idcomanda");
			Order o = new Order(idcomanda,idProdus,idClient, cantitate, total);
			list.add(o);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderBLL:list " + e.getMessage());
		} finally {
			ConnectionFactory.close(listStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return list;
	}

}

