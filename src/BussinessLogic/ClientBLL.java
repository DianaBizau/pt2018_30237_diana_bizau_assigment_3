package BussinessLogic;


import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;


import DataAcces.ConnectionFactory;
import Model.Client;

public class ClientBLL 
{
	
	protected static final Logger LOGGER = Logger.getLogger(ClientBLL.class.getName());
	private static final String insertStatementString = "INSERT INTO client (idClient,nume,adresa)" + " VALUES (?,?,?)";
	private final static String listStatementString = "SELECT * FROM client";
	private final static String deleteStatementString = "DELETE FROM client where idClient = ?";
	private final static String updateStatementString = "UPDATE client SET nume=?, adresa=? where idClient=?";
	
	//operatia de inserare 
	public void insert(int idClient, String nume, String adresa) 
	{
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, idClient);
			insertStatement.setString(2, nume);
			insertStatement.setString(3, adresa);
			insertStatement.executeUpdate();

			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientBLL:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		

}
	
	//operatia de stergere
	public void delete(int idClient)
	{
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement = null;
	
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, idClient);
			int rowsDeleted  = deleteStatement.executeUpdate();
			if(rowsDeleted > 0) System.out.println("Delete");
		
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientBLL:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
			
		}
	}
	
	//operatia de update
	public void update (int idClient, String nume, String adresa)
	{
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement updateStatement = null;

		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setInt(3, idClient);
			updateStatement.setString(1, nume);
			updateStatement.setString(2, adresa);
			int rowsUpdated = updateStatement.executeUpdate();
			if(rowsUpdated > 0 ) System.out.println("Update");
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientBLL:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		
	}
	
	//operatie de listare
	public ArrayList<Client>listare()
	{
		ArrayList<Client> list = new ArrayList<Client>();
		
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement listStatement = null;
		ResultSet rs = null;
	
		try {
			listStatement = dbConnection.prepareStatement(listStatementString, Statement.RETURN_GENERATED_KEYS);
			
			rs = listStatement.executeQuery();
			while(rs.next())
			{
			//conversie
			String nume = rs.getString("nume");
			String adresa = rs.getString("adresa");
			int idClient = rs.getInt("idClient");
			Client c = new Client(idClient,nume,adresa);
			list.add(c);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientBLL:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(listStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return list;
	}
}

