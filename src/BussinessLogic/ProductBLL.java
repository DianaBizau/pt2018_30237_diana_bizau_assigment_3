package BussinessLogic;

import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;


import DataAcces.ConnectionFactory;
import Model.Product;

public class ProductBLL 
{

		protected static final Logger LOGGER = Logger.getLogger(ProductBLL.class.getName());
		private static final String insertStatementString = "INSERT INTO produs (idprodus,nume,pret,cantitate)" + " VALUES (?,?,?,?)";
		private final static String listStatementString = "SELECT* FROM produs";
		private final static String deleteStatementString = "DELETE FROM produs where idprodus = ?";
		private final static String updateStatementString = "UPDATE produs SET nume=?,pret=?,cantitate=? where idprodus=?";
		private final static String searchStatementString = "Select* FROM produs WHERE nume like ?";
		
		public void insert( String nume, int cantitate, float pret, int idprodus)
		{
			Connection dbConnection = ConnectionFactory.getConnection();

			PreparedStatement insertStatement = null;
		
			try {
				insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
				insertStatement.setInt(4, idprodus);
				insertStatement.setString(1, nume);
				insertStatement.setInt(2, cantitate);
				insertStatement.setFloat(3, pret);
				insertStatement.executeUpdate();

			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ProductBLL:insert " + e.getMessage());
			} finally {
				ConnectionFactory.close(insertStatement);
				ConnectionFactory.close(dbConnection);
			}
			

	}
		
		public void delete(int idprodus)
		{
			Connection dbConnection = ConnectionFactory.getConnection();

			PreparedStatement deleteStatement = null;
		
			try {
				deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
				deleteStatement.setInt(1, idprodus);
				int rowsDeleted  = deleteStatement.executeUpdate();
				if(rowsDeleted > 0) System.out.println("Delete");
				
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ProductBLL:delete " + e.getMessage());
			} finally {
				ConnectionFactory.close(deleteStatement);
				ConnectionFactory.close(dbConnection);
				
			}
		}
		
		public void update ( String nume, int cantitate, float pret ,int idprodus)
		{
			Connection dbConnection = ConnectionFactory.getConnection();

			PreparedStatement updateStatement = null;
	
			try {
				updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
				updateStatement.setInt(4, idprodus);
				updateStatement.setString(1, nume);
				updateStatement.setInt(2, cantitate);
				updateStatement.setFloat(3, pret);
				int rowsUpdated = updateStatement.executeUpdate();
				if(rowsUpdated > 0 ) System.out.println("Update");
				
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ProductBLL:update " + e.getMessage());
			} finally {
				ConnectionFactory.close(updateStatement);
				ConnectionFactory.close(dbConnection);
			}
			
		}
		
		public ArrayList<Product>listare()
		{
			ArrayList<Product> list = new ArrayList<Product>();
			
			Connection dbConnection = ConnectionFactory.getConnection();

			PreparedStatement listStatement = null;
			ResultSet rs = null;
		
			try {
				listStatement = dbConnection.prepareStatement(listStatementString, Statement.RETURN_GENERATED_KEYS);	
				rs = listStatement.executeQuery();
				while(rs.next())
				{	
					String nume = rs.getString("nume");
					int cantitate = rs.getInt("cantitate");
					float pret = rs.getFloat("pret");
					int idprodus = rs.getInt("idprodus");
					Product p = new Product(idprodus,nume,pret, cantitate);
					list.add(p);}
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ProdusBLL:update " + e.getMessage());
			} finally {
				ConnectionFactory.close(listStatement);
				ConnectionFactory.close(dbConnection);
			}
			
			return list;
		}
		
		
		public ArrayList<Product> search(String nume)
		{
			Connection dbConnection = ConnectionFactory.getConnection();

			PreparedStatement searchStatement = null;
			nume+= "%";
			ResultSet rs = null;
			ArrayList<Product> list = new ArrayList<Product>();
	
			try {
				searchStatement = dbConnection.prepareStatement(searchStatementString, Statement.RETURN_GENERATED_KEYS);
				
				searchStatement.setString(1, nume);
				rs = searchStatement.executeQuery();
				while(rs.next())	
				{	
					String nume1 = rs.getString("nume");
					int cantitate = rs.getInt("cantitate");
					float pret= rs.getFloat("pret");
					int idprodus = rs.getInt("idprodus");
					Product p = new Product(idprodus,nume1,pret,cantitate);
					list.add(p);
				}
			
				
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ProductBLL:update " + e.getMessage());
			} finally {
				ConnectionFactory.close(searchStatement);
				ConnectionFactory.close(dbConnection);
			}
			return list;
		}
		
	}



